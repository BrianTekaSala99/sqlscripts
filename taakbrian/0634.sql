USE ModernWays;
SELECT Studenten_Id, Cijfer FROM Evaluaties
INNER JOIN Studenten
ON Evaluaties.Studenten_Id = Studenten.Id
Where Cijfer < (select AVG(Cijfer) FROM Evaluaties)
GROUP BY Studenten_Id; 