-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
use ModernWays;
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
-- verifiëren dat de dubbele eruit zijn   
use ModernWays;
select Voornaam, Familienaam from Personen
    order by Voornaam, Familienaam;
    
    
    -- nieuwe tabel Personen. Maar met slechts twee kolommen. Met de alter instructie voegen we de andere kolommen toe, evenals de primary key
    use ModernWays;

alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
   select * from Personen order by Familienaam, Voornaam;
   
   use ModernWays;
alter table Boeken add Personen_Id int not null;

-- We kunnen bekijken hoe we de twee tabellen zullen linken op basis van deze twee kolommen:

select Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
    
    SET SQL_SAFE_UPDATES = 0;
    update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;

-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- het verwijderen van dubbelle kolommen
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
    
    -- dan de constraint toevoegen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);