USE Aptunes;

DROP INDEX VoornaamAchternaamIdx ON Muzikanten;
CREATE INDEX AchternaamVoornaamIdx ON Muzikanten(Familienaam(9), Voornaam(9));
SELECT Voornaam, Familienaam, COUNT(Lidmaatschappen.Muzikanten_Id)
FROM Muzikanten 
INNER JOIN Lidmaatschappen
ON Lidmaatschappen.Muzikanten_Id = Muzikanten.Id
GROUP BY Familienaam, Voornaam
ORDER BY Voornaam, Familienaam;