use ModernWays;
-- toevoegen van jean-paul bij personen omdat hij er nog niet was
INSERT INTO personen(
 voornaam, familienaam)
VALUES(
'Jean-Paul','Sartre');
-- toevoegen van uitgeverij en datum aangespast
insert into Boeken (
   Titel,
   Stad,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id,
   Uitgeverij
)
values (
   'De Woorden',
   'Antwerpen',
   '1961',
   'Een zeer mooi boek.',
   'Roman',
   (select Id from Personen where
       Familienaam = 'Sartre' and Voornaam = 'Jean-Paul'),
       'Bezig bij')