USE Aptunes;

CREATE INDEX VoornaamAchternaamIdx
ON Muzikanten(Voornaam(9), Familienaam(9));