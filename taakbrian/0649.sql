USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
    DECLARE random TINYINT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLSTATE '45002' SELECT 'State 45002 opgevangen. Geen probleem.' ErrorCode;
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
    END;
    
    SET random = FLOOR(RAND() * 3) + 1;
    
    IF random = 1 THEN
        SIGNAL SQLSTATE '45001';
    ELSEIF random = 2 THEN
        SIGNAL SQLSTATE '45002';
    ELSEIF random = 3 THEN
        SIGNAL SQLSTATE '45003';
    END IF;
END$$

DELIMITER ;