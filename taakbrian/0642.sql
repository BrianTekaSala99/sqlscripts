USE aptunes;
DROP PROCEDURE IF EXISTS CleanupOldMembership;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN someDate DATE, OUT numberCleaned INT)
BEGIN
    START TRANSACTION;
    SELECT COUNT(*)
    INTO numberCleaned
    FROM Lidmaatschappen
    WHERE someDate > Einddatum;
    SET sql_safe_updates = 0;
    DELETE FROM Lidmaatschappen
        WHERE someDate > Einddatum;
    SET sql_safe_updates = 1;
    COMMIT;
END $$

DELIMITER ;